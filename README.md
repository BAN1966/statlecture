# StatLecture

This repository contains the slides/notebooks for the statistics lectures. 


## Material

 - lecture 1: [download notebook]();  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.rrz.uni-hamburg.de%2FBAN1966%2Fstatlecture.git/HEAD?filepath=lecture_1.ipynb)
 - lecture 2: [download notebook]();  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.rrz.uni-hamburg.de%2FBAN1966%2Fstatlecture.git/HEAD?filepath=lecture_2.ipynb)
  - lecture 3: [download notebool](); [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.rrz.uni-hamburg.de%2FBAN1966%2Fstatlecture.git/HEAD?filepath=lecture_3.ipynb)


download everything: [zip archive](https://gitlab.rrz.uni-hamburg.de/BAN1966/statlecture/-/archive/main/statlecture-main.zip)